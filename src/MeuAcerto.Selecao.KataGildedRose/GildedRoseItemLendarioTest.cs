﻿using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseItemLendarioTest
    {
        [Fact]
        public void AtualizarQualidade_ValidadePositiva()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = 20, Qualidade = 80 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Sulfuras, a Mão de Ragnaros", Items[0].Nome);
            Assert.Equal(20, Items[0].PrazoValidade);
            Assert.Equal(80, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeNegativa()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = -4, Qualidade = 80 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Sulfuras, a Mão de Ragnaros", Items[0].Nome);
            Assert.Equal(-4, Items[0].PrazoValidade);
            Assert.Equal(80, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeZero()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = 0, Qualidade = 80} };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Sulfuras, a Mão de Ragnaros", Items[0].Nome);
            Assert.Equal(0, Items[0].PrazoValidade);
            Assert.Equal(80, Items[0].Qualidade);
        }
    }
}
