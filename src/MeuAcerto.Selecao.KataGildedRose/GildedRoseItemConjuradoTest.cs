﻿using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseItemConjuradoTest
    {
        [Fact]
        public void AtualizarQualidade_DecrementaValidadeDecrementaQualidade()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Escudo Conjurado", PrazoValidade = 10, Qualidade = 3 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Escudo Conjurado", Items[0].Nome);
            Assert.Equal(9, Items[0].PrazoValidade);
            Assert.Equal(1, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_QualidadeNaoNegativa()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Escudo Conjurado", PrazoValidade = 10, Qualidade = 0 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Escudo Conjurado", Items[0].Nome);
            Assert.Equal(9, Items[0].PrazoValidade);
            Assert.Equal(0, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeIgualUm()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Escudo Conjurado", PrazoValidade = 1, Qualidade = 10 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Escudo Conjurado", Items[0].Nome);
            Assert.Equal(0, Items[0].PrazoValidade);
            Assert.Equal(8, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeIgualZero()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Escudo Conjurado", PrazoValidade = 0, Qualidade = 10 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Escudo Conjurado", Items[0].Nome);
            Assert.Equal(-1, Items[0].PrazoValidade);
            Assert.Equal(6, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeNegativa()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Escudo Conjurado", PrazoValidade = -1, Qualidade = 10 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Escudo Conjurado", Items[0].Nome);
            Assert.Equal(-2, Items[0].PrazoValidade);
            Assert.Equal(6, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeNegativaQualidade1()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Escudo Conjurado", PrazoValidade = -1, Qualidade = 1 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Escudo Conjurado", Items[0].Nome);
            Assert.Equal(-2, Items[0].PrazoValidade);
            Assert.Equal(0, Items[0].Qualidade);
        }
    }
}
