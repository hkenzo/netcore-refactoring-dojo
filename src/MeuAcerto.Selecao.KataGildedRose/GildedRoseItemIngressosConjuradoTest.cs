﻿using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseItemIngressoConjuradoTest
    {
        [Fact]
        public void AtualizarQualidade_DecrementaValidadeIncrementaQualidade()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = 20, Qualidade = 3 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(19, Items[0].PrazoValidade);
            Assert.Equal(5, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_QualidadeNaoNegativa()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = 20, Qualidade = 0 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(19, Items[0].PrazoValidade);
            Assert.Equal(2, Items[0].Qualidade);
        }


        [Fact]
        public void AtualizarQualidade_ValidadeIgualZero()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = 0, Qualidade = 10 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(-1, Items[0].PrazoValidade);
            Assert.Equal(0, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeNegativa()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = -1, Qualidade = 10 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(-2, Items[0].PrazoValidade);
            Assert.Equal(0, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeNegativaQualidade1()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = -1, Qualidade = 1 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(-2, Items[0].PrazoValidade);
            Assert.Equal(0, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeIgual11()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = 11, Qualidade = 1 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(10, Items[0].PrazoValidade);
            Assert.Equal(3, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeMenor11()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = 10, Qualidade = 1 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(9, Items[0].PrazoValidade);
            Assert.Equal(5, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeIgual6()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = 6, Qualidade = 1 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(5, Items[0].PrazoValidade);
            Assert.Equal(5, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeMenor6()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = 5, Qualidade = 1 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(4, Items[0].PrazoValidade);
            Assert.Equal(7, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeMax()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = 20, Qualidade = 50 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(19, Items[0].PrazoValidade);
            Assert.Equal(50, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeMaxValidadeMenor11()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = 7, Qualidade = 49 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(6, Items[0].PrazoValidade);
            Assert.Equal(50, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeMaxValidadeMenor6()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = 3, Qualidade = 49 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(2, Items[0].PrazoValidade);
            Assert.Equal(50, Items[0].Qualidade);
        }

        [Fact]
        public void AtualizarQualidade_ValidadeMaxValidade0()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC Conjurado", PrazoValidade = 0, Qualidade = 50 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Ingressos para o concerto do TAFKAL80ETC Conjurado", Items[0].Nome);
            Assert.Equal(-1, Items[0].PrazoValidade);
            Assert.Equal(0, Items[0].Qualidade);
        }
    }
}
