﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()
        {

            foreach (Item item in Itens.Where(e => e.Nome.Contains("Queijo Brie Envelhecido")).ToList())
            {
                AtualizarQualidadeQueijo(item);
            }

            foreach (Item item in Itens.Where(e => e.Nome.Contains("Ingressos para o concerto do TAFKAL80ETC")).ToList())
            {
                AtualizarQualidadeIngresso(item);
            }

            foreach (Item item in Itens.Where(e => !e.Nome.Contains("Queijo Brie Envelhecido") &&
                        !e.Nome.Contains("Ingressos para o concerto do TAFKAL80ETC") &&
                        !e.Nome.Contains("Sulfuras, a Mão de Ragnaros")).ToList())
            {
                AtualizarQualidadeComum(item);
            }
        }


        private void AtualizarQualidadeComum(Item item)
        {
            item.PrazoValidade -= 1;

            if (item.Qualidade > 0)
            {
                // Se for item conjurado, itemConjurado = 2; itemConjurado = 1 caso contrario
                // Se for validade < 0, passouValidade = 2; passouValidade = 1 caso contrario
                int itemConjurado = item.Nome.Contains("Conjurado") ? 2 : 1;
                int passouValidade = item.PrazoValidade < 0 ? 2 : 1;

                // Quando o (PrazoValidade) do item tiver passado, a (Qualidade) do item diminui duas vezes mais rápido.
                // Os itens "Conjurados" (Conjurado) diminuem a (Qualidade) duas vezes mais rápido que os outros itens.
                item.Qualidade -= passouValidade * itemConjurado;

                //A (Qualidade) do item não pode ser negativa
                if (item.Qualidade < 0)
                {
                    item.Qualidade = 0;
                }
            }
            
        }

        private void AtualizarQualidadeQueijo(Item item)
        {
            item.PrazoValidade -= 1;

            //A (Qualidade) de um item não pode ser maior que 50.
            if (item.Qualidade < 50)
            {
                //O(Queijo Brie envelhecido), aumenta sua qualidade(Qualidade) ao invés de diminuir.
                int passouValidade = item.PrazoValidade < 0 ? 2 : 1;
                int itemConjurado = item.Nome.Contains("Conjurado") ? 2 : 1;
                {
                    item.Qualidade += passouValidade * itemConjurado;
                }

                //A (Qualidade) de um item não pode ser maior que 50.
                if (item.Qualidade > 50)
                {
                    item.Qualidade = 50;
                }
            }
            
        }

        private void AtualizarQualidadeIngresso(Item item)
        {
            item.PrazoValidade -= 1;
            // A (Qualidade) do item vai direto à 0 quando o (PrazoValidade) tiver passado.
            if (item.PrazoValidade < 0)
            {
                item.Qualidade = 0;
            }
            //A (Qualidade) de um item não pode ser maior que 50.
            else if (item.Qualidade < 50)
            {
                int itemConjurado = item.Nome.Contains("Conjurado") ? 2 : 1;
                //A (Qualidade) aumenta em 3 unidades quando o (PrazoValidade) é igual ou menor que 5.
                if (item.PrazoValidade < 5)
                {
                    item.Qualidade += 3 * itemConjurado;
                }
                //A (Qualidade) aumenta em 2 unidades quando o (PrazoValidade) é igual ou menor que 10.
                else if (item.PrazoValidade < 10)
                {
                    item.Qualidade += 2 * itemConjurado;
                }
                else
                {
                    item.Qualidade += itemConjurado;
                }

                //A (Qualidade) de um item não pode ser maior que 50.
                if (item.Qualidade > 50)
                {
                    item.Qualidade = 50;
                }
            }

        }
    }
}
